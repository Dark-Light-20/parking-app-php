<?php
session_start();
if (!isset($_SESSION["id"])) {
    return;
}

$name = $_POST["name"];
$lastname = $_POST["lastname"];
$phone = $_POST["phone"];
$plate = $_POST["plate"];
$brand = $_POST["brand"];
$color = $_POST["color"];
$date = date("Y-m-d H:i");
//
// File
if (isset($_FILES['carimg']) && $_FILES['carimg']['error'] === UPLOAD_ERR_OK) {
    // get details of the uploaded file
    $fileTmpPath = $_FILES['carimg']['tmp_name'];
    $fileName = $_FILES['carimg']['name'];
    $fileSize = $_FILES['carimg']['size'];
    $fileType = $_FILES['carimg']['type'];
    $fileNameCmps = explode(".", $fileName);
    $fileExtension = strtolower(end($fileNameCmps));

    // sanitize file-name
    $newFileName = md5(time() . $fileName) . '.' . $fileExtension;

    // check if file has one of the following extensions
    $allowedfileExtensions = array('jpg', 'png', 'php');

    if (in_array($fileExtension, $allowedfileExtensions)) {
        // directory in which the uploaded file will be moved
        $uploadFileDir = '../cars/';
        $dest_path = $uploadFileDir . $newFileName;

        if(move_uploaded_file($fileTmpPath, $dest_path)) {
            $message ='File is successfully uploaded.';
        }
        else {
            $message = 'There was some error moving the file to upload directory. Please make sure the upload directory is writable by web server.';
            $newFileName = "";
        }
    }
    else {
        $message = 'Upload failed. Allowed file types: ' . implode(',', $allowedfileExtensions);
        $newFileName = "";
    }
}
else {
    $message = 'There is some error in the file upload. Please check the following error.<br>';
    $message .= 'Error:' . $_FILES['carimg']['error'];
    $newFileName = "";
}

// Get BD-Credentials
$BD_DATA = file_get_contents('/opt/lampp/keysmysql/parking-app/credentials.key');
$BD_DATA = json_decode($BD_DATA);
// Connection
$conn = new mysqli("localhost", $BD_DATA->user, $BD_DATA->pswd, $BD_DATA->bd);
if($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
// Query
$sql = "INSERT INTO REGISTROS(date, plate, brand, color, img, p_name, p_last, phone) VALUES(
    '$date','$plate','$brand','$color','$newFileName','$name','$lastname','$phone')";
// Check INSERT
if($conn->query($sql) === TRUE) {
    // New record created successfully
?>

<div class="row justify-content-center text-center">
    <h1 class="card p-1">Registro de ingreso de vehiculo</h1>
</div>
<div class="container card">
    <div class="row">
        <h1>Ingreso a nombre de:</h1>
    </div>
    <div class="row">
        <div class="col">
            <h2><?=$name." ".$lastname?></h2>
        </div>
        <div class="col">
            <h2><?="Contacto: ".$phone?></h2>
        </div>
    </div>
    <div class="row">
        <h1>Vehiculo:</h1>
    </div>
    <div class="row">
        <div class="col">
            <?php 
                $sql2 = "SELECT MARCAS.name as m_name, COLORES.name as c_name FROM MARCAS JOIN COLORES
                        WHERE MARCAS.ID = '$brand' AND COLORES.ID = '$color'";
                $result = $conn->query($sql2);
                if($result->num_rows > 0) {
                    $row = $result->fetch_assoc();
                    $brand = $row["m_name"];
                    $color = $row["c_name"];
                }
            ?>
            <h2><?=$brand.", color: ".$color;?></h2>
            <h2><?="Placa => ".$plate?></h2>
            <div class="row">
                <h1>Fecha de ingreso:</h1>
            </div>
            <div class="row">
                <div class="col">
                    <h2><?=$date?></h2>
                </div>
            </div>
        </div>
        <div class="col">
            <img src="cars/<?=$newFileName?>" class="pic rounded" alt="car.img" height=150>
        </div>
    </div>
    <div class="row">
        <small class="form-text text-muted col col-md-7"><?=$message?></small>
    </div>
</div>
<div class="row justify-content-center mt-1">
    <button class="col btn btn-secondary btn-lg mb-2" id="btnHome">Regresar</button>
</div>

<?php
} else {
    // Error on INSERT
    echo (
    '
    <div class="row justify-content-center text-center">
        <h1 class="card p-1">'."Error: " . $sql . "<br>" . $conn->error.'</h1>
    </div>
    ');
}
// Close connection
$conn->close();
?>