<?php
session_start();
if (!isset($_SESSION["id"])) {
    return;
}
?>

<div class="card row justify-content-center text-center mb-3">
    <h1>Consulta de registros de ingreso</h1>
</div>
<?php
// Get BD-Credentials
$BD_DATA = file_get_contents('/opt/lampp/keysmysql/parking-app/credentials.key');
$BD_DATA = json_decode($BD_DATA);
// Connection
$conn = new mysqli("localhost", $BD_DATA->user, $BD_DATA->pswd, $BD_DATA->bd);
if($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
// Query
$sql = "SELECT date, plate, MARCAS.name as b_name, COLORES.name as c_name, img, p_name, p_last, phone 
        FROM REGISTROS 
        INNER JOIN MARCAS ON(MARCAS.ID = REGISTROS.brand) 
        INNER JOIN COLORES ON (COLORES.ID = REGISTROS.color)";
$results = $conn->query($sql);
// Check results
if ($results->num_rows > 0) {
    // Output data of each row
    echo('<div class="row justify-content-around">');
    while($row = $results->fetch_assoc()) {
        //Card
        echo('
            <div class="card col-12 col-md-4 p-3 mb-2 mr-2" style="width: 18rem;">
                <img src="cars/'.$row["img"].'" class="pic card-img-top rounded" alt="carimg.jpg" height=150>
                <div class="card-body">
                    <h5 class="card-title">'.$row["plate"].'</h5>
                    <h6 class="card-title">Nombre de propietario: '.$row["p_name"].' '.$row["p_last"].'</h6>
                    <h6 class="card-title">Contacto: '.$row["phone"].'</h6>
                    <h6 class="card-title">Marca: '.$row["b_name"].'</h6>
                    <h6 class="card-title">Color: '.$row["c_name"].'</h6>
                    <h5 class="card-title mt-3">Fecha de ingreso: '.$row["date"].'</h5>
                </div>
            </div>
        ');
    }
    echo('</div>');
}
// Close connection
$conn->close();
?>
<div class="row justify-content-center text-center mt-3 mb-3">
    <button class="col-10 btn btn-secondary btn-lg" id="btnHome">Regresar</button>
</div>