<?php
if (isset($_POST["actualpswd"])) {
    session_start();
    if (isset($_SESSION["id"])) {
        $user = $_SESSION["id"];
        $actual = $_POST["actualpswd"];
        if (isset($_POST["newpswd1"]) && isset($_POST["newpswd2"])) {
            $pss1 = $_POST["newpswd1"];
            $pss2 = $_POST["newpswd2"];
            if ($pss1 != "" && $pss1 == $pss2) {
                $pss1 = password_hash($pss1, PASSWORD_DEFAULT);
                // Get BD-Credentials
                $BD_DATA = file_get_contents('/opt/lampp/keysmysql/parking-app/credentials.key');
                $BD_DATA = json_decode($BD_DATA);
                // Connection
                $conn = new mysqli("localhost", $BD_DATA->user, $BD_DATA->pswd, $BD_DATA->bd);
                if($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                // Query
                $sql = "UPDATE USUARIOS SET password=? WHERE ID=?;";
                $stmt = $conn->prepare($sql);
                $stmt->bind_param("ss", $pss1, $user);
                if ($stmt->execute()) {
                    echo("yes");
                } else {
                    echo "Falló la actualización: (" . $stmt->errno . ") " . $stmt->error;
                }
                // Close connection
                $conn->close();
            }
        }
    }
}
?>