<?php
if (!isset($_POST["username"])) {
    return;
}
$user = $_POST["username"]; 
$pass = $_POST["password"];

// Get BD-Credentials
$BD_DATA = file_get_contents('/opt/lampp/keysmysql/parking-app/credentials.key');
$BD_DATA = json_decode($BD_DATA);
// Connection
$conn = new mysqli("localhost", $BD_DATA->user, $BD_DATA->pswd, $BD_DATA->bd);
if($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
// Query
$sql = "SELECT ID, password FROM USUARIOS WHERE username=?;";
$stmt = $conn->prepare($sql);
$stmt->bind_param("s", $user);
$stmt->execute();
$result = $stmt->get_result();
// Check
if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();
    // Verify password
    if (password_verify($pass, $row["password"])) {
        session_start();
        $_SESSION["id"] = $row["ID"];
        $_SESSION["user"] = $user;
        $_SESSION["pass"] = $pass;
        echo($user);
    } else {
        echo("no");
    }
} else {
    echo("no");
}
// Close connection
$conn->close();
?>