<?php
if (isset($_POST["username"])) {
    $user = $_POST["username"];
    if (isset($_POST["password1"]) && isset($_POST["password2"])) {
        $pss1 = $_POST["password1"];
        $pss2 = $_POST["password2"];
        if ($pss1 != "" && $pss1 == $pss2) {
            $pss1 = password_hash($pss1, PASSWORD_DEFAULT);
            // Get BD-Credentials
            $BD_DATA = file_get_contents('/opt/lampp/keysmysql/parking-app/credentials.key');
            $BD_DATA = json_decode($BD_DATA);
            // Connection
            $conn = new mysqli("localhost", $BD_DATA->user, $BD_DATA->pswd, $BD_DATA->bd);
            if($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
            // Query
            $sql = "INSERT INTO USUARIOS(username, password) VALUES(?, ?);";
            $stmt = $conn->prepare($sql);
            $stmt->bind_param("ss", $user, $pss1);
            if ($stmt->execute()) {
                echo("yes");
            } else {
                echo "Falló el registro: (" . $stmt->errno . ") " . $stmt->error;
            }
            // Close connection
            $conn->close();
        }
    }
}
?>