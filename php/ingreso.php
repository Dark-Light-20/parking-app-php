<?php
session_start();
if (!isset($_SESSION["id"])) {
    return;
}
?>

<div class="row justify-content-center text-center">
    <h1 class="card p-1">Formulario de ingreso de vehiculo</h1>
</div>
<div class="row justify-content-between">
    <div class="col col-md-5">
        <h2 class="card">Datos de Vehiculo:</h2>
    </div>
    <div class="col col-md-5">
        <h2 class="card">Datos de Propietario:</h2>
    </div>
</div>
<form id="formReg" action="registrar.php" method="POST" enctype="multipart/form-data">
    <!-- Placa, Nombres y Apellidos del propietario, Color, Fecha de ingreso y Fotografia. -->
    <div class="row justify-content-between">
        <div class="col col-md-5 card">
            <div class="form-group mt-1">
                <label for="plate"><b>Placa de vehiculo:</b></label>
                <input type="text" class="form-control col-8 col-md-5" name="plate" id="plate" maxlength=6 placeholder="ABC123" required>
            </div>
            <div class="form-group">
                <label for="brand"><b>Marca de vehiculo:</b></label>
                <select class="form-control col col-md-7" name="brand" id="brand" required>
                    <?php 
                        // Get BD-Credentials
                        $BD_DATA = file_get_contents('/opt/lampp/keysmysql/parking-app/credentials.key');
                        $BD_DATA = json_decode($BD_DATA);
                        // Connection
                        $conn = new mysqli("localhost", $BD_DATA->user, $BD_DATA->pswd, $BD_DATA->bd);
                        if($conn->connect_error) {
                            die("Connection failed: " . $conn->connect_error);
                        }
                        // Query
                        $sql = "SELECT * FROM MARCAS";
                        $results = $conn->query($sql);
                        // Check results
                        if ($results->num_rows > 0) {
                            // Output data of each row
                            while($row = $results->fetch_assoc()) {
                                echo ('<option value="'.$row["ID"].'">'.$row["name"].'</option>');
                            }
                        }
                    ?>
                </select>
                </div>
            <div class="form-group">
                <label for="color"><b>Color de vehiculo:</b></label>
                <select class="form-control col col-md-7" name="color" id="color" required>
                    <?php
                        $sql = "SELECT * FROM COLORES";
                        $results = $conn->query($sql);
                        // Check results
                        if ($results->num_rows > 0) {
                            // Output data of each row
                            while($row = $results->fetch_assoc()) {
                                echo ('<option value="'.$row["ID"].'">'.$row["name"].'</option>');
                            }
                        }
                        // Close connection
                        $conn->close();
                    ?>
                </select>
            </div>
            <div class="form-group ">
                <label for="carimg"><b>Imagen de vehiculo:</b></label>
                <input type="file" class="form-control-file" id="carimg" name="carimg" required>
            </div>
        </div>
        <div class="col col-md-5 card">
            <div class="form-group mt-1">
                <label for="name"><b>Nombres:</b></label>
                <input type="text" class="form-control col-lg-8" name="name" id="name" required>
            </div>
            <div class="form-group">
                <label for="lastname"><b>Apellidos:</b></label>
                <input type="text" class="form-control col-lg-8" name="lastname" id="lastname" required>
            </div>
            <div class="form-group">
                <label for="phone"><b>Número de contacto:</b></label>
                <input type="tel" class="form-control col col-md-6" id="phone" name="phone" pattern="[0-9]{10}" placeholder="Ej: 3002224455" required>
            </div>
        </div>
    </div>
    <div class="row justify-content-center mt-2">
        <input type="submit" class="col btn btn-primary btn-lg" name="uploadBtn" id="uploadBtn" value="Registrar">
    </div>
</form>
<div class="row justify-content-center mt-1">
    <button class="col btn btn-secondary btn-lg mb-2" id="btnHome">Regresar</button>
</div>