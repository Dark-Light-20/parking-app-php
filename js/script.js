$(document).ready(() => {
  // HTML Vars
  title = $("#title");
  container = $("#container");

  // Home function
  function login() {
    // Home AJAX
    $.ajax({
      type: "GET",
      url: "html/login.html",
      success: loginLoad,
      error: handleError,
    });
  }

  function loginLoad(data) {
    title.html("Parking App Login");
    container.html(data);

    $('#login-form').on('submit', (e) => {
      e.preventDefault();
      var loginData = new FormData($('#login-form')[0]);
      $.ajax({
        type: "POST",
        url: "php/login.php",
        data: loginData,
        processData: false,
        contentType: false,
        success: loginSubmit,
        error: handleError
      });
    });

    $('#btn-sigin').click(() => {
      $.ajax({
        type: "GET",
        url: "html/sigin.html",
        success: siginLoad,
        error: handleError,
      });
    });
  }

  function loginSubmit(data) {
    if (data !== "no") {
      $.ajax({
        type: "GET",
        url: "html/navbarMenu.html",
        success: navbarMenuLoad,
        error: handleError
      });
      $("#navbar-title").html("Bienvenido! "+data);
      home();
    } else {
      $("#log-container").html(
        "<div class='alert alert-danger' role='alert'>"+
        "Datos incorrectos!"+
        "</div>");
    }
  }

  function navbarMenuLoad(data) {
    // Get navbar Menu
    let dropdownMenu = $("<div>");
    dropdownMenu.addClass("collapse navbar-collapse");
    dropdownMenu.html(data);
    $("#navbar").append(dropdownMenu);
    // Logout
    $("#btn-logout").click((e) => {
      $.ajax({
        type: "GET",
        url: "php/logout.php",
        success: login,
        error: handleError
      });
      $("#navbar > div").last().remove(); 
      $("#navbar-title").html("Parking-App");
    });
    // Change Password
    $("#btn-changePswd").click((e) => {
      $.ajax({
        type: "GET",
        url: "html/changePswd.html",
        success: changePswd,
        error: handleError
      });
    });
  }

  function siginLoad(data) {
    title.html("Parking App Registry");
    loadData(data);
    $("#btnHome").click(login);

    $('#sigin-form').on('submit', (e) => {
      e.preventDefault();
      if ($('#password1').val() === $('#password2').val()) {
        var siginData = new FormData($('#sigin-form')[0]);
        $.ajax({
          type: "POST",
          url: "php/sigin.php",
          data: siginData,
          processData: false,
          contentType: false,
          success: siginSubmit,
          error: handleError
        });
      } else {
        $("#log-container").html(
          "<div class='alert alert-danger' role='alert'>"+
          "Las contraseñas no coinciden!"+
          "</div>");
      }
    });
  }
  
  function siginSubmit(data) {
    if (data === "yes") {
      $.ajax({
        type: "GET",
        url: "html/success.html",
        success: (e) => {
          container.html(data);
          $('#btn-go-login').click(login);
        },
        error: handleError
      });
    } else {
      $("#log-container").html(
        "<div class='alert alert-danger' role='alert'>"+
        "Error en los datos al registrar!"+
        "</div>");
    }
  }

  function changePswd(data) {
    title.html("Cambiar contraseña");
    loadData(data);

    $('#changePswd-form').on('submit', (e) => {
      e.preventDefault();
      if ($('#newpswd1').val() === $('#newpswd2').val()) {
        var siginData = new FormData($('#changePswd-form')[0]);
        $.ajax({
          type: "POST",
          url: "php/changePswd.php",
          data: siginData,
          processData: false,
          contentType: false,
          success: changePswdSubmit,
          error: handleError
        });
      } else {
        $("#log-container").html(
          "<div class='alert alert-danger' role='alert'>"+
          "Las contraseñas no coinciden!"+
          "</div>");
      }
    });
  }

  function changePswdSubmit(data) {
    if (data === "yes") {
      $.ajax({
        type: "GET",
        url: "html/successPswd.html",
        success: loadData,
        error: handleError
      });
    } else {
      $("#log-container").html(
        "<div class='alert alert-danger' role='alert'>"+
        "Error en los datos al actualizar!"+
        "</div>");
    }
  }

  // Home function
  function home() {
    // Home AJAX
    $.ajax({
      type: "GET",
      url: "html/body.html",
      success: homeLoad,
      error: handleError,
    });
  }

  function homeLoad(data) {
    // Home HTML data
    title.html("Parking App");
    container.html(data);
    // Buttons AJAX
    // btnIng
    $("#btnIng").click(() => {
      $.ajax({
        type: "GET",
        url: "php/ingreso.php",
        success: ingresoLoad,
        error: handleError,
      });
    });
    // btnCon
    $("#btnCon").click(() => {
      $.ajax({
        type: "GET",
        url: "php/consulta.php",
        success: (data) => {
          title.html("Consulta de Vehiculos");
          loadData(data);
        },
        error: handleError,
      });
    });
  }

  function ingresoLoad(data) {
    title.html("Registro de Vehiculo");
    loadData(data);

    $('#formReg').on('submit', (e) => {
      e.preventDefault();
      var formData = new FormData($('#formReg')[0]);
      $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "php/registrar.php",
        data: formData,
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success: (data) => {
          title.html("Registro exitoso");
          loadData(data);
        },
        error: handleError
      });
    });
  }

  function loadData(data) {
    // Load HTML data in container div
    container.html(data);
    // Reload Home button
    $("#btnHome").click(home);
  }

  // AJAX error log function
  function handleError(jqXHR, textStatus, error) {
    console.log(error);
  }

  login();
});
